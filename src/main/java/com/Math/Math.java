package com.Math;

public class Math {
    public static void main(String[] args) {
        Math math=new Math();
        System.out.println(math.taskDegrees(25,10));
        System.out.println(math.taskRad(0,10));
        System.out.println(math.task2(7,4,2,1));
        System.out.println(math.task3(-0.5,0));
        System.out.println(math.task4(0));
    }
    public double taskDegrees(double injection, double speed){
        double Rad = injection * 3.14 / 180.0;
        return taskRad(Rad,speed);
    }
    public double taskRad(double injection, double speed){
        if(speed < 0){
            System.out.println("Отрицательная скорость");
            return 0;
        }
        if(injection < 0 || injection > 3.14 / 2){
            System.out.println("Неправильный угол броска");
            return 0;
        }
        double mOnSecond = speed / 3.6;
        double result = (2.0 * mOnSecond * mOnSecond * java.lang.Math.sin(injection) * java.lang.Math.sin(injection)) / 9.8;
        return result;
    }
    public double task2( double speed1, double speed2, double distance, double time){
        if(distance < 0 || speed1 < 0 || speed2 < 0 || time < 0){
            System.out.println("Отрицательные вводные даные");
            return 0;
        }
        return distance + (speed1 + speed2) * time;
    }
    public String task3(double x, double y){
        double z=2/3d;
        double k=2*y/3;
        if(x >= y && x <= (k + z) && x >= 0||-x >= y && -x <= (k + z) && x <= 0){
            return "Точка лежит в заштрихованой области";
        }
        return "Точка не лежит в заштрихованой области";
    }
    public Double task4(double x){
        double a = java.lang.Math.exp(x + 1) + 2 * java.lang.Math.exp(x) * java.lang.Math.cos(x);
        double b = x - java.lang.Math.exp(x + 1) * java.lang.Math.exp(x);
        double rez = (6 * java.lang.Math.log1p(java.lang.Math.sqrt(a)))
                / (java.lang.Math.log1p(b)) + java.lang.Math.abs(java.lang.Math.cos(x)
                / java.lang.Math.exp(java.lang.Math.sin(x)));
        return rez;
    }
}

