package com.Math;

import com.Math.Math;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class MathTest {
    Math math = new Math();
    static Arguments[] degreesTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1,4,7.666358662413815E-5),
                Arguments.arguments(25,10,0.28098509721801307),
                Arguments.arguments(45,10,0.787)
        };
    }

    @ParameterizedTest
    @MethodSource("radTestArgs")
    void degrees(double injection,double speed, double expected) {
        double actual = math.taskRad(injection, speed);
        assertTrue(java.lang.Math.abs(expected - actual) < 1);
    }
    static Arguments[] radTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3.14/4,10,0.787),
                Arguments.arguments(1,4,0.17840096202407943),
                Arguments.arguments(0,10,0.0)
        };
    }

    @ParameterizedTest
    @MethodSource("radTestArgs")
    void degrees1(double injection,double speed, double expected) {
        double actual = math.taskRad(injection,speed);
        assertTrue(java.lang.Math.abs(expected - actual) < 0.001d);
    }

    static Arguments[] task2TestArgs(){
        return new Arguments[]{
                Arguments.arguments(5,10,5,0,5),
                Arguments.arguments(1,2,3,4,15),
                Arguments.arguments(7,4,2,1,13)
        };
    }
    @ParameterizedTest
    @MethodSource("task2TestArgs")
    void task2(double speed1,double speed2, double distance,double time,double expected) {
        double actual = math.task2(speed1, speed2, distance, time);
        assertTrue(java.lang.Math.abs(expected - actual) < 0.1d);
    }
    static Arguments[] task3TestArgs(){
        return new Arguments[]{
                Arguments.arguments(-0.5,0,"Точка лежит в заштрихованой области"),
                Arguments.arguments(1,1,"Точка лежит в заштрихованой области"),
                Arguments.arguments(2,-2,"Точка не лежит в заштрихованой области")
        };
    }
    @ParameterizedTest
    @MethodSource("task3TestArgs")
    void task3(double x, double y, String expected){
        String actual = math.task3(x, y);
        assertEquals(expected,actual);
    }
    static Arguments[] task4TestArgs(){
        return new Arguments[]{
                Arguments.arguments(0.1,1),
                Arguments.arguments(-0.1,1),
                Arguments.arguments(0.1,2)
        };
    }

    @ParameterizedTest
    @MethodSource("task4TestArgs")
    void task4(double k,double p){
        double x=p* java.lang.Math.PI-k;
        assertTrue(math.task4(x).isNaN());
    }

}